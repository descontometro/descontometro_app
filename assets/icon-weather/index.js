module.exports = {
    cloud: require('./cloud.svg'),
    cloudy: require('./cloudy.svg'),
    lightningstorm: require('./lightningstorm.svg'),
    moon: require('./moon.svg'),
    rain: require('./rain.svg'),
    shiningsun: require('./shiningsun.svg'),
    snowflake: require('./snowflake.svg'),
    storm: require('./storm.svg'),
    thermometer: require('./thermometer.svg'),
    wind: require('./wind.svg')
}