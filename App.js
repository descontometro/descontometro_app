import React from 'react';
import Router from './app/config/router';
import { Provider, connect } from 'react-redux';
import { addNavigationHelpers } from 'react-navigation';
import store from './store';
import { Font, AppLoading } from 'expo';

import axios from 'axios';
//axios.defaults.baseURL = 'https://vaccaro.o2.ag/node/';
axios.defaults.baseURL = 'http://192.168.43.37:8000/api/';
axios.defaults.headers.common['Authorization'] = 'qrqwerqwer';

import * as firebase from 'firebase';

firebase.initializeApp({
    apiKey: "AIzaSyB142Qt7QJvpN25bINg36g-hdeZV3xQMck",
    authDomain: "push-app-vaccaro.firebaseapp.com",
    databaseURL: "https://push-app-vaccaro.firebaseio.com",
    projectId: "push-app-vaccaro",
    storageBucket: "push-app-vaccaro.appspot.com",
    messagingSenderId: "623530615051"
})

const App = ({ dispatch, sideMenu}) => (
    <Router
        navigation={addNavigationHelpers({
            dispatch,
            state: sideMenu,
    })}
    />
);

const mapStateToProps = (state) => ({
	sideMenu: state.sideMenu
})

const AppWithNavigation = connect(mapStateToProps)(App);

class Root extends React.Component {
    constructor() {
        super();

        this.state = {
            isReady: false
        };
    }
    componentWillMount() {
        this.loadFonts();
        this.loginApp();
    }

    loginApp = () => {
        firebase.auth().signInAnonymously().then((result) => console.log(result)).catch((err) => console.log(err))
    }

    async loadFonts() {
        await Font.loadAsync({
            "raleway-light": require('./assets/fonts/Raleway-Light.ttf'),
            "raleway": require('./assets/fonts/Raleway-Regular.ttf'),
            "raleway-semi-bold": require('./assets/fonts/Raleway-SemiBold.ttf'),
            "raleway-bold": require('./assets/fonts/Raleway-Bold.ttf'),
            "raleway-extra-bold": require('./assets/fonts/Raleway-ExtraBold.ttf'),
            "FontAwesome": require('./assets/fonts/fontawesome-webfont.ttf')
        });
        this.setState({ isReady: true });
    }
    componentDidMount() {

    }

    render(){
        if (!this.state.isReady) {
            return <AppLoading />;
        }
        return (
            <Provider store={store()}>
                <AppWithNavigation />
            </Provider>
        );
    }
};

export default Root;
