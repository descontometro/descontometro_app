import React from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import 'moment/locale/pt-br';
moment.locale('pt-BR');

import { getCotacaoMilho, getCotacaoTrigo, getCotacaoSoja } from '../../redux/action/cotacaoAction';

class CotacaoTrigoTemplate extends React.Component {
	constructor (props) {
		super(props);

		this.state = {};
	}

	componentWillMount(){

	}

	componentDidMount() {
        this.props.dispatch(getCotacaoTrigo());
	}

	componentWillReceiveProps(nextProps){

    }

	render () {
        const { cotacao } = this.props;
		return (
            <View style={styles.container}>

                <ScrollView style={styles.content}>
					{/* <View style={styles.containerCotacao}>
						{
							this.props.cotacao && this.props.cotacao.map((item, index) => (
								<View style={[styles.containerTitle, {marginTop: index != 0 ? 10 : 0}]} key={index}>
									<View style={[styles.containerTitle, {alignItems: 'center', flexDirection: 'column'}]}>
										<Text style={{fontFamily: 'raleway-extra-bold', fontSize: 22, color: '#b2b2b2'}}>{moment(item.periodo).format('DD')}</Text>
										<Text style={{fontFamily: 'raleway-extra-bold', fontSize: 15, color: '#b2b2b2'}}>{moment(item.periodo).format('MMM').toLocaleUpperCase()}</Text>
									</View>

									<View style={[styles.containerTitle, {alignItems: 'center'}]}>
										<Text style={[styles.containerCotacao_font, {fontSize: 16}]}>R$ {item.valor}</Text>
									</View>

									<View style={[styles.containerTitle, {alignItems: 'center'}]}>
										{
											item.variacao_dia.indexOf('-') <= -1 ?
											<Icon name="long-arrow-up" color="#44BA09" style={{marginLeft: 8}} /> :
											<Icon name="long-arrow-down" color="#de0606" style={{marginLeft: 8}} />
										}
										<Text style={{color: item.variacao_dia.indexOf('-') <= -1 ? '#44BA09' : '#de0606', fontFamily: 'raleway-light', marginLeft: 3}}>
                                            {item.variacao_dia}
										</Text>
									</View>
								</View>
							))
						}
					</View> */}
					<View style={{flex: 1, justifyContent: 'center'}}>
						<View>
							<Text style={{color: '#44BA09', fontFamily: 'raleway-semi-bold', fontSize: 23}}>Aqui o cliente Vaccaro terá acesso em primeira mão a cotação exclusiva, pensada e praticada apenas para quem é amigo de verdade.</Text>

							<TouchableOpacity
								style={{justifyContent: 'flex-end', flexDirection: 'row', alignItems: 'center', marginTop: 10}}
								onPress={() => this.props.navigation.navigate('HomeScreen')}>
								<Icon name="arrow-left" size={13} color="#44BA09" />
								<Text style={{fontFamily: 'raleway-light', fontSize: 18, color: '#878787', marginLeft: 5}}>Voltar</Text>
							</TouchableOpacity>
						</View>
					</View>
                </ScrollView>

                <View style={styles.border_bottom} />
            </View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
        flex: 1,
        backgroundColor: '#f2f2f2',
        flexDirection: 'column',
	},
	border_bottom: {
        width: '100%',
        height: 5,
        backgroundColor: '#44BA09',
        marginTop: 'auto'
    },
    content: {
        paddingHorizontal: 30,
		paddingBottom: 20,
		flexDirection: 'column',
	},
	containerTitle: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		backgroundColor: '#FFF',
		paddingVertical: 5,
		paddingHorizontal: 10
	},
	containerTitle_barra: {
		width: '30%',
        height: 5,
        backgroundColor: '#44BA09',
	},
	containerTitle_title: {
		color: '#44BA09',
		fontFamily: 'raleway-semi-bold',
		fontSize: 25,
		marginTop: 5
	},
	containerTitle_data: {
		color: '#CE4B0A',
		fontFamily: 'raleway-light',
		fontSize: 14,
		marginTop: 5,
		textAlign: 'right'
	},
	containerCotacao: {
		borderRadius: 5,
		paddingVertical: 15,
	},
	containerCotacao_font: {
		color: '#878787',
		fontFamily: 'raleway-semi-bold',
	},
});

const mapStateToProps = function(state){
  return {
    cotacao: state.cotacao.listMilho
  }
}

export default connect(mapStateToProps)(CotacaoTrigoTemplate);
