import React from 'react';
import { View, StyleSheet, Text, Button, TouchableOpacity, Animated } from 'react-native';
import { TabNavigator } from 'react-navigation';
import PropTypes from 'prop-types';
import moment from 'moment';

import CotacaoMilho from './cotacaoMilho'
import CotacaoTrigo from './cotacaoTrigo'
import CotacaoSoja from './cotacaoSoja'

const styles = StyleSheet.create({
	headerOptions: {
		flex: 0,
		flexDirection: 'row',
		height: 60,
		marginTop: 15,
		elevation: 6,
		backgroundColor: '#FFF',
	},
	button: {
		flex: 1,
	    alignSelf: 'stretch',
		alignItems: 'center',
	},
	viewButton: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center'
	},
	textButton: {
		color: '#44BA09',
		fontSize: 16,
		fontFamily: 'raleway-light',
	},
	barRunning: {
		width: 40,
		height: 4,
		backgroundColor: '#44BA09',		
		position: 'absolute',
		bottom: 0,
	},
	containerTitle: {
		flexDirection: 'row', 
		justifyContent: 'space-between'
	},
	containerTitle_barra: {
		width: '30%',
        height: 5,
        backgroundColor: '#44BA09',
	},
	containerTitle_title: {
		color: '#44BA09',
		fontFamily: 'raleway-semi-bold',
		fontSize: 25,
		marginTop: 5
	},
	containerTitle_data: {
		color: '#CE4B0A',
		fontFamily: 'raleway-light',
		fontSize: 14,
		marginTop: 5,
		textAlign: 'right'
	},
});

class TabsTemplate extends React.Component {
	constructor(props){
		super(props);

		this.state = {obrigacoesDimensions: PropTypes.object, compromissosDimensions: PropTypes.object, sojaDimensions: PropTypes.object};

		this.animatedValue = new Animated.Value(0)
	}

	componentWillReceiveProps(nextProps){

		if(typeof nextProps.navigation.state.routes[0].params !== 'undefined'){
			console.log(nextProps.navigation.state.routes[0].params.pag)
			if(nextProps.navigation.state.routes[0].params.pag == 'Milho'){
				this.props.navigation.navigate('CotacaoMilho')
			}else if(nextProps.navigation.state.routes[0].params.pag == 'Trigo'){
				this.props.navigation.navigate('CotacaoTrigo')
			}else if(nextProps.navigation.state.routes[0].params.pag == 'Soja'){
				this.props.navigation.navigate('CotacaoSoja')
			}
			nextProps.navigation.state.routes[0].params = {}
		}

		if(nextProps.navigationState){
			const spacingBar = (this.state.obrigacoesDimensions.width - 40) / 2;

			if(nextProps.navigationState.index === 0){
				Animated.timing(this.animatedValue, {
					toValue: spacingBar,
					duration: 600
				}).start();
			}else if(nextProps.navigationState.index === 1){
				Animated.timing(this.animatedValue, {
					toValue: spacingBar + this.state.compromissosDimensions.x,
					duration: 600
				}).start();
			}else if(nextProps.navigationState.index === 2){
				Animated.timing(this.animatedValue, {
					toValue: spacingBar + this.state.sojaDimensions.x,
					duration: 600
				}).start();
			}
		}
	}

	render(){
		return (
			<View style={{backgroundColor: '#f2f2f2', paddingHorizontal: 30, paddingVertical: 20}}>
				<View style={styles.containerTitle}>
					<View>
						<View style={styles.containerTitle_barra} />
						<Text style={styles.containerTitle_title}>Cotação</Text>
					</View>

					<View>
						<Text style={styles.containerTitle_data}>{moment(new Date()).format('DD/MM/YYYY')}</Text>
					</View>
				</View>

				<View style={styles.headerOptions}>
					<Animated.View style={[styles.barRunning, {transform: [{translateX: this.animatedValue}]}]} />
					<TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate('CotacaoMilho')} style={styles.button} onLayout={(event) => this.setState({obrigacoesDimensions: event.nativeEvent.layout})}>
						<View style={styles.viewButton}>
							<Text style={styles.textButton}>Milho</Text>
						</View>
					</TouchableOpacity>
					<View style={{width: 1, height: 45, backgroundColor: '#eaeaea', top: 8}} />
					<TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate('CotacaoTrigo')} style={styles.button} onLayout={(event) => this.setState({compromissosDimensions: event.nativeEvent.layout})}>
						<View style={styles.viewButton}>
							<Text style={styles.textButton}>Trigo</Text>
						</View>
					</TouchableOpacity>
					<View style={{width: 1, height: 45, backgroundColor: '#eaeaea', top: 8}} />
					<TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate('CotacaoSoja')} style={styles.button} onLayout={(event) => this.setState({sojaDimensions: event.nativeEvent.layout})}>
						<View style={styles.viewButton}>
							<Text style={styles.textButton}>Soja</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>
		);
	}
}

export default TabNavigator(
	{
		CotacaoMilho: {
			screen: CotacaoMilho,
			navigationOptions: {
				tabBarLabel: 'Milho'
			}
		},
		CotacaoTrigo: {
			screen: CotacaoTrigo,
			navigationOptions: {
				tabBarLabel: 'Trigo'
			}
		},
		CotacaoSoja: {
			screen: CotacaoSoja,
			navigationOptions: {
				tabBarLabel: 'Soja'
			}
		},
	},
	{
		tabBarComponent: (props) => <TabsTemplate {...props} />
	}
);
