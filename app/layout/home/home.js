import React from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, Image, ActivityIndicator } from 'react-native';
import { Constants, Location, Permissions } from 'expo';
import SvgUri from 'react-native-svg-uri';
import { getCuponsAction } from '../../redux/action/cuponsAction';

import IconWeather from '../../../assets/icon-weather'

class HomeScreen extends React.Component {
	constructor (props) {
		super(props);

		this.state = {location: null, errorMessage: null};
	}

	componentWillMount(){
		this.props.dispatch(getCuponsAction());
	}

	componentDidMount() {
	}

	componentWillReceiveProps(nextProps){

	}

	_cutString = (string) => {
		return string.toString().substring(0,2);
	}

	render () {
		const resizeMode = 'contain';

		return (
      <ScrollView>
				<View style={styles.container}>
					{
						this.props.cupons.couponsList.length ? this.props.cupons.couponsList.map((item, index) => (
							<View key={index} style={styles.bloco01} >
								<TouchableOpacity style={[styles.bloco01_link]} onPress={() => this.props.navigation.navigate('CupomScreen',{id: item.id})}>
									<View style={styles.bloco01_container}>
										<Image
											source={{uri: 'http://192.168.43.37:8000/storage/coupons/'+item.image}}
       								style={{ flex:1, opacity: 0.8, resizeMode, width: '100%', height: 250}}  />
										<Text style={styles.bloco01_text}>{item.name}</Text>
									</View>
									<View style={styles.bloco01_bar} />
								</TouchableOpacity>
							</View>
						)) : <View style={{flex: 1, justifyContent: 'center'}}><ActivityIndicator size={30} color="#a9c761" /></View>
					}
				</View>

				<View style={{backgroundColor: '#FFF', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingVertical: 15}}>
				</View>

				<View style={styles.border_bottom} />
      </ScrollView>
		)
	}
}
const styles = StyleSheet.create({
	container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
    paddingHorizontal: 0,
    paddingVertical: 0
	},
	border_bottom: {
		borderBottomWidth: 5,
		borderColor: '#44BA09',
	},
	bloco01: {
		flexDirection: 'row',
		paddingVertical: 5,
	},
	bloco01_link: {
		flex: 1,
		flexDirection: 'column',
		flexWrap: 'wrap',
		marginHorizontal: 5,
		backgroundColor: '#000'
	},
	bloco01_container: {
		alignItems: 'center',
		justifyContent: 'center'
	},
	bloco01_text: {
		marginTop: 25,
		fontFamily: 'raleway-semi-bold',
		fontSize: 40,
		color: '#f71b1b',
		position: 'absolute',
		fontWeight: '900',
		backgroundColor: 'rgba(0, 0, 0, 0.65)',
		marginHorizontal: 10,
		textAlign: 'center'
	},
	bloco01_bar: {
		width: '100%',
		height: 6,
		backgroundColor: '#44BA09',
		position: 'absolute',
		bottom: 0,
		alignSelf: 'center'
	}
});

const mapStateToProps = function(state){
	return {
		cupons: state.cupons
	}
}

export default connect(mapStateToProps)(HomeScreen);
