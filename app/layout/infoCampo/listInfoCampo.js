import React from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet, TouchableOpacity, WebView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import HTMLView from 'react-native-htmlview';

import moment from 'moment';
import 'moment/locale/pt-br';
moment.locale('pt-BR');

class ListInfoCampo extends React.Component {
	constructor (props) {
        super(props);
    }

	componentWillMount(){
        const { dispatch, data } = this.props;

        if(data.descricao_info_campo.length > 50){
            data.descricao_info_campo = data.descricao_info_campo.replace(/<.+?>/g, '').slice(0,50)
            data.descricao_info_campo += '...'
        }else{
            data.descricao_info_campo = data.descricao_info_campo.replace(/<.+?>/g, '')
        }
	}
    
	componentDidMount() {

    }
    
	componentWillReceiveProps(nextProps){        
        if(nextProps.data.descricao_info_campo.length > 50){
            nextProps.data.descricao_info_campo = nextProps.data.descricao_info_campo.replace(/<.+?>/g, '').slice(0,50)
            nextProps.data.descricao_info_campo += '...'
        }else{
            nextProps.data.descricao_info_campo = nextProps.data.descricao_info_campo.replace(/<.+?>/g, '')
        }
    }
    
	render () {
        const { dispatch, data } = this.props;

		return (
            <TouchableOpacity 
                style={[styles.container, {borderLeftWidth: 2, borderLeftColor: data.color_info_campo_tipo}]}
                onPress={() => dispatch(NavigationActions.navigate({routeName: 'InfoCampoContentScreen', params: {id: data.id_info_campo}}))}>
                <View style={{alignItems: 'center', marginHorizontal: 15}}>
                    <Text style={{fontFamily: 'raleway-extra-bold', fontSize: 22, color: '#b2b2b2'}}>{moment(data.data_comeco_info_campo).format('DD')}</Text>
					<Text style={{fontFamily: 'raleway-extra-bold', fontSize: 15, color: '#b2b2b2'}}>{moment(data.data_comeco_info_campo).format('MMM').toLocaleUpperCase()}</Text>
                </View>

                <View style={{flex: 1}}>
                    <Text style={{color: data.color_info_campo_tipo, fontFamily: 'raleway-semi-bold', fontSize: 16}}>{data.titulo_info_campo}</Text>                
                    <HTMLView
                        value={data.descricao_info_campo}
                    />
                </View>

                <View style={{marginHorizontal: 15}}>
                    <Icon name="plus-circle" size={20} color="#b2b2b2" />
                </View>
            </TouchableOpacity>
		)
	}
}

const styles = StyleSheet.create({
	container: {
        flex: 0,
		flexDirection: 'row',
		alignItems: 'center',
		paddingVertical: 10,
	}
});

const mapStateToProps = function(state){
  return { }
}

export default connect(mapStateToProps)(ListInfoCampo);
