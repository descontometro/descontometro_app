import React from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, Image, Picker, Button, DatePickerAndroid, ActivityIndicator, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import ListInfoCampo from './listInfoCampo';
import { listInfoCampo, listTiposInfoCampo } from '../../redux/action/infoCampoAction'

class InfoCampoScreen extends React.Component {
	constructor (props) {
        super(props);
        
        const today = new Date();
		const day = today.getDate() < 10 ? `0${today.getDate()}` : today.getDate();
        const month = (today.getMonth() + 1) < 10 ? `0${today.getMonth() + 1}` : today.getMonth() + 1;
        const dateToday = `${day}/${month}/${today.getFullYear()}`;

		this.state = {
            language: '',
            date_start: dateToday,
            date_end: dateToday,
            listInfoCampo: [],
            listHasLoaded: false
		}
	}

	componentWillMount(){
		
	}
    
	componentDidMount() {
        this.props.dispatch(listInfoCampo())
        this.props.dispatch(listTiposInfoCampo())
    }
    
	componentWillReceiveProps(nextProps){
        let { infoCampo } = nextProps;

        if(infoCampo.listInfoCampo){
            this.setState({listHasLoaded: true})
            if(infoCampo.listInfoCampo.length > 0){
                this.setState({listInfoCampo: infoCampo.listInfoCampo})
            }
        }
    }

    _datePicker = async (dateState) => {        
        try {
            const {action, year, month, day} = await DatePickerAndroid.open({
                date: new Date()
            });

            if (action !== DatePickerAndroid.dismissedAction) {
                let newDay = day < 10 ? `0${day}` : day;
                let newMonth = (month + 1) < 10 ? `0${(month + 1)}` : (month + 1);
                switch(dateState){
                    case 'date_start':
                        this.setState({date_start: `${newDay}/${newMonth}/${year}`});
                    break;
                    case 'date_end':
                        this.setState({date_end: `${newDay}/${newMonth}/${year}`});
                    break;
                }
                
            }
        }catch ({code, message}) {
            console.warn('Cannot open date picker', message);
        }
    }

    _sortList = (typeInfo) => {
        if(typeInfo == null) return this.setState({listInfoCampo: this.props.infoCampo.listInfoCampo})
        this.setState({listInfoCampo: this.props.infoCampo.listInfoCampo.filter((item) => item.id_info_campo_tipo === typeInfo)})
    }

    _keyExtractor = (item, index) => item.id_info_campo;
    
	render () {        
		return (
            <View style={styles.container}>
                <ScrollView contentContainerStyle={styles.content}>
					<View style={styles.containerTitle}>
						<View>
							<View style={styles.containerTitle_barra} />
							<Text style={styles.containerTitle_title}>InfoCampo</Text>
						</View>
					</View>

                    <View style={[styles.containerInfoCampo, {display: 'none'}]}>
                        <Text>Intervalo de datas</Text>
                        <View>
                            <View style={[styles.containerTitle, {flex: 1}]}>

                                <TouchableOpacity onPress={() => this._datePicker('date_start')} style={{flex: 1, marginRight: 5, borderWidth: 2, borderColor: '#44BA09', borderRadius: 15, paddingVertical: 5, alignItems: 'center'}}>
                                    <Text style={{color: '#878787'}}>{this.state.date_start}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this._datePicker('date_end')} style={{flex: 1, marginLeft: 5, borderWidth: 2, borderColor: '#44BA09', borderRadius: 15, paddingVertical: 5, alignItems: 'center'}}>
                                    <Text style={{color: '#878787'}}>{this.state.date_end}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View style={{flex: 1}}>
                            <Picker
                                selectedValue={this.state.language}
                                onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
                                <Picker.Item label="Noticias" value="noticias" />
                                <Picker.Item label="Artigos" value="artigos" />
                                <Picker.Item label="Eventos" value="eventos" />
                            </Picker>
                        </View>
                    </View>

                    <View style={{flexDirection: 'row', marginTop: 15}}>
                        {!this.props.infoCampo.listTiposInfoCampo.length > 0 && <View style={{flex: 1, justifyContent: 'center'}}><ActivityIndicator size={25} color="#E5AB0B" /></View>}
						{
							this.props.infoCampo.listTiposInfoCampo.map((item, index) => (
								<TouchableOpacity key={index} style={{flexDirection: 'row', marginRight: 10}} onPress={() => this._sortList(item.id_info_campo_tipo)}>
                                    <View style={{marginTop: 3, marginRight: 5, width: 10, height: 10, backgroundColor: item.color_info_campo_tipo}} />
                                    <Text style={{fontFamily: 'raleway-light', color: '#00000050'}}>{item.nome_info_campo_tipo}</Text>
                                </TouchableOpacity>
							))
                        }
                        <TouchableOpacity style={{flexDirection: 'row', marginRight: 10}} onPress={() => this._sortList(null)}>
                            <View style={{marginTop: 3, marginRight: 5, width: 10, height: 10, backgroundColor: '#00000080'}} />
                            <Text style={{fontFamily: 'raleway-light', color: '#00000050'}}>Todas</Text>
                        </TouchableOpacity>
                    </View>
                    
					<View style={{backgroundColor: '#FFF', marginTop: 15}}>
                        <Text>{JSON.stringify(this.state.listInfoCampo.hasLoaded)}</Text>
                        {this.state.listInfoCampo.length > 0 || !this.state.listHasLoaded && <View style={{flex: 1, justifyContent: 'center'}}><ActivityIndicator size={70} color="#E5AB0B" /></View>}
                        {this.state.listInfoCampo.length == 0 && this.state.listHasLoaded && <Text style={{color: '#44BA09', fontFamily: 'raleway-semi-bold', fontSize: 16, textAlign: 'center', padding: 20}}>Não há cadastros no momento</Text>}
                        <FlatList
                            data={this.state.listInfoCampo}
                            keyExtractor={this._keyExtractor}
                            renderItem={({item}) => <ListInfoCampo data={item} />}
                            />
					</View>
                </ScrollView>

                <View style={styles.border_bottom} />				
            </View>			
		)
	}
}

const styles = StyleSheet.create({
	container: {
        flex: 1,
        backgroundColor: '#f2f2f2',
        flexDirection: 'column',
	},
	border_bottom: {
        width: '100%',
        height: 5,
        backgroundColor: '#44BA09',
        marginTop: 'auto'
    },
    content: {
        paddingHorizontal: 30, 
        paddingTop: 20,
		flexDirection: 'column',
	},
	containerTitle: {
		flexDirection: 'row', 
		justifyContent: 'space-between'
	},
	containerTitle_barra: {
		width: '30%',
        height: 5,
        backgroundColor: '#44BA09',
	},
	containerTitle_title: {
		color: '#44BA09',
		fontFamily: 'raleway-semi-bold',
		fontSize: 25,
		marginTop: 5
	},
	containerTitle_data: {
		color: '#CE4B0A',
		fontFamily: 'raleway-light',
		fontSize: 14,
		marginTop: 5,
		textAlign: 'right'
	},
	containerInfoCampo: {
		backgroundColor: '#FFF',
		borderRadius: 5,
		paddingHorizontal: 10,
		paddingVertical: 15,
		marginTop: 15
	},
	containerInfoCampo_font: {
		color: '#878787',
		fontFamily: 'raleway-semi-bold',
	},
});

const mapStateToProps = function(state){
    return {
        infoCampo: state.infoCampo.toJS()
    }
}

export default connect(mapStateToProps)(InfoCampoScreen);
