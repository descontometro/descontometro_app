import React from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, Image, WebView, Modal, Dimensions, ActivityIndicator, NetInfo} from 'react-native';
import { TabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import HTMLView from 'react-native-htmlview';

import moment from 'moment';
import 'moment/locale/pt-br';
moment.locale('pt-BR');

import { getSelectInfoCampo } from '../../redux/action/infoCampoAction'


class ModalImage extends React.Component {
    constructor(props){
        super(props)

        this.state = {openModal: false}
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.openModal){
            this.setState({openModal: nextProps.openModal})            
        }
    }

    doCallBack(){        
        this.setState({openModal: false})
        this.props.callBack(false)
    }

    render(){
        return (
            <Modal 
                visible={this.state.openModal} 
                onRequestClose={() => this.props.callBack(this.state.openModal)} 
                animationType={'slide'} 
                hardwareAccelerated={true}
                transparent={true}>
                <TouchableOpacity 
                    onPress={() => this.doCallBack()} 
                    activeOpacity={.9}
                    style={{flex: 1, backgroundColor: 'rgba(0,0,0,.9)'}} >
                    <Icon name="times" size={30} color={"#FFF"} style={{position: 'absolute', right: 15, top: 15}} />
                    <View style={{flex: 1, padding: 15}}>
                        <Image source={{uri: this.props.URI, isStatic: true}} style={{flex: 1, resizeMode: Image.resizeMode.contain}} />
                    </View>
                </TouchableOpacity>
            </Modal>
        )
    }
}

class InfoCampoContentScreen extends React.Component {
	constructor (props) {
        super(props);

        this.state = {openModal: false, imageModal: null, showThumbs: false}
	}

	componentWillMount(){
        const ID = this.props.navigation.state.params.id;
        this.props.dispatch(getSelectInfoCampo(ID))

        NetInfo.getConnectionInfo().then((connectionInfo) => {
            if(connectionInfo.type == 'wifi' || connectionInfo.type == 'cellular'){
                this.setState({showThumbs: true})
            }
        });
	}
    
	componentDidMount() {

    }    
    
	componentWillReceiveProps(nextProps){
        //console.log(nextProps.content)        
    }
    componentWillUnmount(){
        this.props.dispatch({type: 'DESTROY_INFO_CAMPO'})
    }

    chanceModalState = (result) => {
        console.log(result);
    }
    
	render () {
        let { content, isOffline } = this.props;

		if(content.length){
            return (
                <View style={styles.container}>
                    
                    <ModalImage openModal={this.state.openModal} URI={this.state.imageModal} callBack={(result) => this.setState({openModal: result})}/>

                    <ScrollView contentContainerStyle={styles.content}>
                        <View style={styles.containerTitle}>
                            <View>
                                <View style={styles.containerTitle_barra} />
                                <Text style={styles.containerTitle_title}>InfoCampo</Text>
                            </View>
                        </View>
    
                        <View style={[styles.containerText, {borderLeftColor: content[0].color_info_campo_tipo}]}>
                            <View style={styles.containerText_data}>
                                <Text style={[styles.containerText_data_font, {fontSize: 22}]}>{moment(content[0].data_comeco_info_campo).format('DD')}</Text>
                                <Text style={[styles.containerText_data_font, {fontSize: 15}]}>{moment(content[0].data_comeco_info_campo).format('MMM').toLocaleUpperCase()}</Text>
    
                                {
                                    content[0].data_termina_info_campo && 
                                    (
                                    <View style={{alignItems: 'center', }}>
                                        <Text style={[styles.containerText_data_font, {marginVertical: 10}]}>Até</Text>
                                        <Text style={[styles.containerText_data_font, {fontSize: 22}]}>{moment(content[0].data_termina_info_campo).format('DD')}</Text>
                                        <Text style={[styles.containerText_data_font, {fontSize: 15}]}>{moment(content[0].data_termina_info_campo).format('MMM').toLocaleUpperCase()}</Text>
                                    </View>
                                    )
                                }
                            </View>
    
                            <View style={styles.containerText_content}>
                                <Text style={[styles.containerText_content_title, {color: content[0].color_info_campo_tipo}]}>{content[0].titulo_info_campo}</Text>
                                <HTMLView
                                    value={content[0].descricao_info_campo}
                                />
    
                                {
                                    content[0].video_info_campo &&
                                    <WebView
                                        style={{flex:1, height: 180, marginTop: 25}}
                                        javaScriptEnabled={true}
                                        source={{uri: content[0].video_info_campo}}
                                    />
                                }
                            </View>
                        </View>
    
                        {/* <TouchableOpacity style={{flex: 1, borderWidth: 2, borderColor: '#E5AB0B', paddingVertical: 15, borderRadius: 30, justifyContent: 'center', marginBottom: 25, flexDirection: 'row', alignItems: 'center'}}>
                            <Text style={{fontFamily: 'raleway-semi-bold', fontSize: 18, color: '#44BA09'}}>Confirma presença</Text>
                        </TouchableOpacity> */}
                        
                        {this.state.showThumbs && 
                            <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between', marginHorizontal: -3}}>
                                {
                                    content[0].images.map((item, index) => (
                                        <TouchableOpacity 
                                            key={index} 
                                            onPress={() => this.setState({openModal: true, imageModal: item.maior_info_campo_galeria})}
                                            style={{width: 70, height: 70, borderWidth: 2, borderColor: '#44BA09', marginVertical: 5, marginHorizontal: 3}}>
                                                <Image source={{uri: item.thumb_info_campo_galeria}} style={{flex: 1, width: undefined, height: undefined}} />
                                        </TouchableOpacity>
                                    ))
                                }
                            </View>
                        }
    
                        <TouchableOpacity 
                            style={{flex: 1,justifyContent: 'flex-end', flexDirection: 'row', alignItems: 'center'}}
                            onPress={() => this.props.navigation.navigate('InfoCampoScreen')}>
                            <Icon name="arrow-left" size={13} color="#44BA09" />
                            <Text style={{fontFamily: 'raleway-light', fontSize: 18, color: '#878787', marginLeft: 5}}>Voltar</Text>
                        </TouchableOpacity>
                    </ScrollView>
    
                    <View style={styles.border_bottom} />				
                </View>			
            )
        }else{
            if(!isOffline){
                return (
                    <View style={{flex: 1, justifyContent: 'center'}}><ActivityIndicator size={70} color="#E5AB0B" /></View>
                )
            }else{
                return (
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <View style={{padding: 20}}>
                            <Text style={{color: '#44BA09', fontFamily: 'raleway-semi-bold', fontSize: 23}}>Você está sem conexão e não tem esse conteudo salvo em seu celular</Text>
                            <TouchableOpacity
                                style={{justifyContent: 'flex-end', flexDirection: 'row', alignItems: 'center'}}
                                onPress={() => this.props.navigation.navigate('InfoCampoScreen')}>
                                <Icon name="arrow-left" size={13} color="#44BA09" />
                                <Text style={{fontFamily: 'raleway-light', fontSize: 18, color: '#878787', marginLeft: 5}}>Voltar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )
            }            
        }
	}
}

const styles = StyleSheet.create({
	container: {
        flex: 1,
        backgroundColor: '#f2f2f2',
        flexDirection: 'column',
	},
	border_bottom: {
        width: '100%',
        height: 5,
        backgroundColor: '#44BA09',
        marginTop: 'auto'
    },
    content: {
        paddingHorizontal: 30, 
		paddingVertical: 20,
		flexDirection: 'column',
	},
	containerTitle: {
		flexDirection: 'row', 
		justifyContent: 'space-between'
	},
	containerTitle_barra: {
		width: '30%',
        height: 5,
        backgroundColor: '#44BA09',
	},
	containerTitle_title: {
		color: '#44BA09',
		fontFamily: 'raleway-semi-bold',
		fontSize: 25,
		marginTop: 5
    },
    containerText: {
        borderLeftWidth: 2,
        backgroundColor: '#FFF', 
        flexDirection: 'row', 
        paddingVertical: 15, 
        marginVertical: 15,
        borderRadius: 5
    },
    containerText_data: {
        alignItems: 'center', 
        marginHorizontal: 15
    },
    containerText_data_font: {
        fontFamily: 'raleway-extra-bold',
        color: '#b2b2b2'
    },
    containerText_content: {
        flex: 1, 
        marginRight: 15
    },
    containerText_content_title: {         
        fontFamily: 'raleway-semi-bold', 
        fontSize: 16
    },
    containerText_content_text: {
        fontFamily: 'raleway-light', 
        color: '#878787', 
        fontSize: 13
    }
});

const mapStateToProps = function(state){
    return { 
        content: state.infoCampo.toJS().selectedInfoCampo,
        isOffline: state.infoCampo.toJS().isOffline
    }
}

export default connect(mapStateToProps)(InfoCampoContentScreen);
