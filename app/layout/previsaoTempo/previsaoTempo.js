import React from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, Image, ActivityIndicator } from 'react-native';
import { Constants, Location, Permissions } from 'expo';
import SvgUri from 'react-native-svg-uri';
import moment, { locale } from 'moment';
moment.locale('pt-BR');

import IconWeather from '../../../assets/icon-weather';
import { getPrevisaTempo } from '../../redux/action/previsaoTempoAction';

class RenderIcon extends React.Component {
	constructor (props) {
		super(props);
	}

	render() {
        let iconName = this.props.icon != undefined ? this.props.icon.replace('.svg', '') : 'thermometer'
		let ImageRender = IconWeather[iconName];
		return (
			<SvgUri
				width={this.props.size}
				height={this.props.size}
				fill={this.props.index == 0 ? '#FFF' : '#b2b2b2'}
				source={ImageRender} />
		)
	}
}

class PrevisaoTempoScreen extends React.Component {
	constructor (props) {
		super(props);

		this.state = {errorMessage: null};
	}

	componentWillMount(){
		this._getLocationAsync();
	}

	componentDidMount() {

	}

	componentWillReceiveProps(nextProps){
				
    }

    _getLocationAsync = async () => {
		let { status } = await Permissions.askAsync(Permissions.LOCATION);
		if (status !== 'granted') {
			this.setState({
				errorMessage: 'Permissão para acessar a localização atual negada.',
			});
		}

		Location.getProviderStatusAsync().then(async (result) => {
			if(result.gpsAvailable){
				let location = await Location.getCurrentPositionAsync({});
				this.props.dispatch(getPrevisaTempo({latitude: location.coords.latitude, longitude: location.coords.longitude}));
			}else{
				this.setState({
					errorMessage: 'GPS está desativado.',
				});
			}
		})
    };
    
    _cutString = (string) => {
		return string.toString().substring(0,2);
	}
    
	render () {
        //{tempo_agora, location, previsao_tempo}
        const { previsaoTempo } = this.props

        if(previsaoTempo.length && this.state.errorMessage == null){
            return (
                <View style={styles.container}>
                    
                    <View style={{backgroundColor: '#81c2e5', width: '100%', height: 190, position: 'absolute'}} />
    
                    <ScrollView style={styles.content}>
                        <Text style={{color: '#FFF', fontFamily: 'raleway-light', fontSize: 15, marginLeft: 20}}>{moment(new Date()).format('DD/MM/YYYY')} | {previsaoTempo[0].dados_localizacao.cidade}</Text>
    
                        <View style={[styles.previsao_tempo_view]}>
                            <View>
                                <Image
                                    style={{width: 45, height: 45}}
                                    source={{uri: previsaoTempo[0].data[0].clima[0].image}}
                                />
                            </View>
                            
                            <View style={{flex: 5, alignItems: 'center'}}>
                                <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 5}}>
                                    <Text style={[styles.previsao_tempo_text_semibold_white, {fontSize: 20}]}>{this._cutString(previsaoTempo[0].data[0].temp_min)}°</Text>
                                    <Text style={[styles.previsao_tempo_text_semibold_white, {fontSize: 20}]}>{this._cutString(previsaoTempo[0].data[0].temp_max)}°</Text>
                                </View>
                                <Text style={[styles.previsao_tempo_text_light_white, {fontSize: 16}]}>{moment(previsaoTempo[0].data[0].data).format('dddd').replace('-feira', '')}</Text>
                            </View>
                            <View style={{flex: 5}}>
                                <Text style={[styles.previsao_tempo_text_light_white, {fontSize: 14}]}>Chuva</Text>
                                <Text style={[styles.previsao_tempo_text_light_white, {fontSize: 16, fontFamily: 'raleway-semi-bold'}]}>{previsaoTempo[0].data[0].chuva}%</Text>
                            </View>

                            <View style={{flex: 5}}>
                                <Text style={[styles.previsao_tempo_text_light_white, {fontSize: 14}]}>Umidade</Text>
                                <Text style={[styles.previsao_tempo_text_light_white, {fontSize: 16, fontFamily: 'raleway-semi-bold'}]}>{previsaoTempo[0].data[0].umidade}%</Text>
                            </View>                   
                        </View>
    
                        <View style={{backgroundColor: '#f2f2f2', borderRadius: 5}}>
                            {
                                previsaoTempo[0].data.filter((item, index) => index != 0).map((item, index) => (
                                    <View style={[styles.previsao_tempo_view]} key={index}>
                                        <View>
                                            <Image
                                                style={{width: 45, height: 45}}
                                                source={{uri: item.clima[0].image}}
                                            />
                                        </View>
    
                                        <View style={{flex: 5, alignItems: 'center'}}>
                                            <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: 5}}>
                                                <Text style={[styles.previsao_tempo_text_semibold_normal, {fontSize: 20}]}>{this._cutString(item.temp_min)}°</Text>
                                                <Text style={[styles.previsao_tempo_text_semibold_normal, {fontSize: 20}]}>{this._cutString(item.temp_max)}°</Text>
                                            </View>
                                            <Text style={[styles.previsao_tempo_text_light_normal, {fontSize: 16}]}>{moment(item.data).format('dddd').replace('-feira', '')}</Text>
                                        </View>
    
                                        <View style={{flex: 5}}>
                                            <Text style={[styles.previsao_tempo_text_light_normal, {fontSize: 14}]}>Chuva</Text>
                                            <Text style={[styles.previsao_tempo_text_light_normal, {fontSize: 16, fontFamily: 'raleway-semi-bold'}]}>{item.chuva}%</Text>
                                        </View>

                                        <View style={{flex: 5}}>
                                            <Text style={[styles.previsao_tempo_text_light_normal, {fontSize: 14}]}>Umidade</Text>
                                            <Text style={[styles.previsao_tempo_text_light_normal, {fontSize: 16, fontFamily: 'raleway-semi-bold'}]}>{item.umidade}%</Text>
                                        </View>
                                    </View>
                                ))
                            }
                        </View>
                        
                    </ScrollView>
    
                    <View style={styles.border_bottom} />				
                </View>			
            )
        }else{
            if(this.state.errorMessage != null){
                return <View style={{flex: 1, justifyContent: 'center'}}><Text style={{color: '#44BA09', fontFamily: 'raleway-semi-bold', fontSize: 23, textAlign: 'center'}}>{this.state.errorMessage}</Text></View>
            }else{
                return <View style={{flex: 1, justifyContent: 'center'}}><ActivityIndicator size={70} color="#E5AB0B" /></View>
            }
        }
		
	}
}

const styles = StyleSheet.create({
	container: {
        flex: 1,
        backgroundColor: '#f2f2f2',
        flexDirection: 'column',
	},
	border_bottom: {
        width: '100%',
        height: 5,
        backgroundColor: '#44BA09',
        marginTop: 'auto'
    },
    content: {
        paddingHorizontal: 30, 
        paddingVertical: 20
    },
    previsao_tempo_view: {
		flexDirection: 'row',
		justifyContent: 'space-between',
        alignItems: 'center',
        margin: 20
	},
	previsao_tempo_link: {
		flex: 1,
		marginHorizontal: 5,
		alignItems: 'center',
		borderRadius: 5,
		paddingVertical: 10
	},
	previsao_tempo_text_semibold_white: {
		color: '#FFF',
		fontFamily: 'raleway-semi-bold'
	},
	previsao_tempo_text_semibold_normal: {
		color: '#b2b2b2',
		fontFamily: 'raleway-semi-bold'
	},
	previsao_tempo_text_light_white: {
		color: '#FFF',
		fontFamily: 'raleway-light'
	},
	previsao_tempo_text_light_normal: {
		color: '#b2b2b2',
		fontFamily: 'raleway-light'
	},
});

const mapStateToProps = function(state){
  return {
    previsaoTempo: state.previsaoTempo.previsaoTempo
  }
}

export default connect(mapStateToProps)(PrevisaoTempoScreen);
