import React from 'react';
import { connect } from 'react-redux';
import { Dimensions, View, StyleSheet, Text, Image, KeyboardAvoidingView, Alert, Keyboard, TouchableOpacity, Animated, TextInput, TouchableWithoutFeedback, ActivityIndicator } from 'react-native';
import { NavigationActions } from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome';
import { FileSystem, Font } from 'expo';

import { postLogin } from '../../redux/action/loginAction'


class Login extends React.Component {
	constructor (props) {Animated
		super(props);

		this.state = {
			user: 'everton@o2multi.com.br',
			password: '123456',
			msgModal: '',
			opacityBemVindo: new Animated.Value(1),
			isBemVindo: 'flex',
			opacityLogIn: new Animated.Value(0),
			isLogIn: 'none',
			isEdit: true
		};
	}

	componentWillMount(){
	}

	componentDidMount() {

	}

	componentWillReceiveProps(nextProps){
		if(nextProps.user.isAutorized){
			nextProps.navigation.navigate('HomeScreen');
		}else{
			Alert.alert(`Usuário ou senha incorreto.`);
			this.setState({isEdit: true})
		}
	}

	_setOpacity = () => {
		const { showModal, opacityBemVindo, opacityLogIn } = this.state;

		const animationTime = 300;

		Animated.timing(opacityBemVindo, {
			toValue: 0,
			duration: animationTime,
			useNativeDriver: true
		  }
		).start();

		setTimeout(() => {
			this.setState({isLogIn: 'flex', isBemVindo: 'none'})

			Animated.timing(opacityLogIn, {
				toValue: 1,
				duration: animationTime,
				useNativeDriver: true
			  }
			).start();
		}, animationTime + 50)
	}

	_submit = () => {
		Keyboard.dismiss()
		this.setState({isEdit: false});
		this.props.dispatch(postLogin({user: this.state.user, password: this.state.password}))
	}

	render () {
		const { dispatch } = {...this.props};

		return (

			<KeyboardAvoidingView style={{flex: 1}}>
				<Image source={require('../../../assets/background-inicial.jpg')} resizeMode="stretch" style={{position: 'absolute', bottom: 0}} />

				<TouchableWithoutFeedback>
					<Animated.View style={[styles.containerForm]}>
						<View>
							<Text style={{fontFamily: 'raleway-light', fontSize: 24, color: '#44BA09', marginTop: -50, textAlign: 'center'}}>SEJA BEM-VINDO</Text>
							<Text style={{fontFamily: 'raleway-extra-bold', fontSize: 26, color: '#44BA09', marginBottom: 40, textAlign: 'center'}}>AO DESCONTOMETRO!</Text>
						</View>
						<View style={styles.containerInput}>
							<View style={styles.viewInput}>
								<Icon name="user" size={25} color="#9d9d9c40" />
								<TextInput
									value={this.state.user}
									style={styles.input}
									placeholder={'Usuário'}
									placeholderTextColor={'#9d9d9c'}
									underlineColorAndroid={'transparent'}
									onChangeText={(text) => this.setState({user: text})}
									editable={this.state.isEdit} />
							</View>

							<View style={styles.viewInput}>
								<Icon name="key" size={25} color="#9d9d9c40" />
								<TextInput
									value={this.state.password}
									secureTextEntry
									style={styles.input}
									placeholder={'Senha'}
									placeholderTextColor={'#9d9d9c'}
									underlineColorAndInputTextroid={'transparent'}
									onChangeText={(text) => this.setState({password: text})}
									editable={this.state.isEdit} />
							</View>

							<View style={{marginTop: 20}}>
								<TouchableOpacity
									onPress={() => this._submit()}
									style={{borderWidth: 2, borderColor: '#44BA09', paddingVertical: 15, alignItems: 'center', borderRadius: 30}}>
									{this.state.isEdit && <Text style={{fontFamily: 'raleway-light', fontSize: 20, color: '#44BA09'}}>FAZER LOGIN</Text>}
									{!this.state.isEdit && <ActivityIndicator size="large" color="#44BA09" />}
								</TouchableOpacity>
							</View>
						</View>
					</Animated.View>
				</TouchableWithoutFeedback>

	    </KeyboardAvoidingView>

		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		flexDirection: 'column',
        justifyContent: 'space-between'
	},
	containerForm: {
		flex: 1,
		alignItems: 'center',
		flexDirection: 'column',
		justifyContent: 'center'
	},
	containerInput: {
		width: Dimensions.get('window').width - 40,
		elevation: 10,
		backgroundColor: '#FFF',
		borderRadius: 5,
		paddingHorizontal: 20,
		paddingVertical: 40
	},
	viewInput: {
		flex: 0,
		flexDirection: 'row',
		alignItems: 'center',
		borderBottomWidth: 1,
		borderColor: '#9d9d9c'
	},
	input: {
		backgroundColor: '#FFF',
		height: 60,
		flex: 1,
		marginLeft: 15
	}

});

const mapStateToProps = function(state){
  return {
    user: state.login
  }
}

export default connect(mapStateToProps)(Login);
