import React from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, Image, WebView, Modal, Dimensions, ActivityIndicator, NetInfo} from 'react-native';
import { TabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

class faleGtcScreen extends React.Component {
	constructor (props) {
        super(props);

        this.state = {openModal: false, imageModal: null, showThumbs: false}
	}

	componentWillMount(){

	}

	componentDidMount() {

    }

	componentWillReceiveProps(nextProps){

    }
    componentWillUnmount(){

    }

	render () {
        return (
            <View style={{flex: 1, justifyContent: 'center'}}>
                <View style={{padding: 20}}>
                    <Text style={{color: '#44BA09', fontFamily: 'raleway-semi-bold', fontSize: 23}}>Esse será o canal direto de conversa do Amigo Vaccaro com sua assistência técnica em um canal exclusivo de comunicação.</Text>

                    <TouchableOpacity
                        style={{justifyContent: 'flex-end', flexDirection: 'row', alignItems: 'center', marginTop: 10}}
                        onPress={() => this.props.navigation.navigate('HomeScreen')}>
                        <Icon name="arrow-left" size={13} color="#44BA09" />
                        <Text style={{fontFamily: 'raleway-light', fontSize: 18, color: '#878787', marginLeft: 5}}>Voltar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
	}
}

const styles = StyleSheet.create();

const mapStateToProps = function(state){
    return {
        content: state.infoCampo.toJS().selectedInfoCampo,
        isOffline: state.infoCampo.toJS().isOffline
    }
}

export default connect(mapStateToProps)(faleGtcScreen);
