import Expo, { SQLite } from 'expo';
import axios from 'axios';
import { NetInfo } from 'react-native';

const db = SQLite.openDatabase('vaccaro.db');

db.transaction(tx => {
    tx.executeSql('CREATE TABLE IF NOT EXISTS cotacao (id INTEGER PRIMARY KEY AUTOINCREMENT, type char, value text);');    
    //tx.executeSql('DROP TABLE cotacao');
});

export function getCotacaoMilho() {     
    return async function(dispatch){

        const netInfo = await NetInfo.getConnectionInfo();

        if(netInfo.type == 'wifi' || netInfo.type == 'cellular'){            
            axios.get('/cotacao/listMilhoCotacao')
            .then((result) => {
                db.transaction(
                    (tx) => {
                        tx.executeSql('SELECT * FROM cotacao WHERE type = ?', ['listMilho'], (_, { rows }) => {
                            if(rows.length > 16){
                                tx.executeSql('DELETE FROM cotacao WHERE type = ?', ['listMilho'])
                            }
                        });                        
                        tx.executeSql('INSERT INTO cotacao (type, value) values (?, ?)', ['listMilho', JSON.stringify(result.data)]);
                    }
                );                
                dispatch({type: 'LIST_MILHO', payload: result.data});
            })
            .catch((err) => {
                console.error(err)
            })
        }else{
            db.transaction(
                (tx) => {
                    tx.executeSql('SELECT * FROM cotacao WHERE type = ? ORDER BY id DESC LIMIT 1', ['listMilho'], (_, { rows }) => {
                        dispatch({type: 'LIST_MILHO', payload: JSON.parse(rows._array[0].value)});
                    });
                },
            );
        }
    }
}

export function getCotacaoTrigo() {     
    return async function(dispatch){

        const netInfo = await NetInfo.getConnectionInfo();
        
        if(netInfo.type == 'wifi' || netInfo.type == 'cellular'){            
            axios.get('/cotacao/listTrigoCotacao')
            .then((result) => {
                db.transaction(
                    (tx) => {
                        tx.executeSql('SELECT * FROM cotacao WHERE type = ?', ['listTrigo'], (_, { rows }) => {
                            if(rows.length > 16){
                                tx.executeSql('DELETE FROM cotacao WHERE type = ?', ['listTrigo'])
                            }
                        });
                        tx.executeSql('INSERT INTO cotacao (type, value) values (?, ?)', ['listTrigo', JSON.stringify(result.data)]);
                    }
                );                
                dispatch({type: 'LIST_TRIGO', payload: result.data});
            })
            .catch((err) => {
                console.error(err)
            })
        }else{
            db.transaction(
                (tx) => {
                    tx.executeSql('SELECT * FROM cotacao WHERE type = ? ORDER BY id DESC LIMIT 1', ['listTrigo'], (_, { rows }) => {
                        dispatch({type: 'LIST_TRIGO', payload: JSON.parse(rows._array[0].value)});
                    });
                },
            );
        }
    }
}

export function getCotacaoSoja() {     
    return async function(dispatch){

        const netInfo = await NetInfo.getConnectionInfo();
        
        if(netInfo.type == 'wifi' || netInfo.type == 'cellular'){            
            axios.get('/cotacao/listSojaCotacao')
            .then((result) => {
                db.transaction(
                    (tx) => {
                        tx.executeSql('SELECT * FROM cotacao WHERE type = ?', ['listSoja'], (_, { rows }) => {
                            if(rows.length > 16){
                                tx.executeSql('DELETE FROM cotacao WHERE type = ?', ['listSoja'])
                            }
                        });
                        tx.executeSql('INSERT INTO cotacao (type, value) values (?, ?)', ['listSoja', JSON.stringify(result.data)]);
                    }
                );                
                dispatch({type: 'LIST_SOJA', payload: result.data});
            })
            .catch((err) => {
                console.error(err)
            })
        }else{
            db.transaction(
                (tx) => {
                    tx.executeSql('SELECT * FROM cotacao WHERE type = ? ORDER BY id DESC LIMIT 1', ['listSoja'], (_, { rows }) => {
                        dispatch({type: 'LIST_SOJA', payload: JSON.parse(rows._array[0].value)});
                    });
                },
            );
        }
    }
}