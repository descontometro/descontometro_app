import Expo, { SQLite } from 'expo';
import axios from 'axios';
import { NetInfo } from 'react-native';
import moment, { locale } from 'moment';
moment.locale('pt-BR');

import CodeWeather from '../../component/codeYahooWeather'

const db = SQLite.openDatabase('vaccaro.db');

db.transaction(tx => {
    tx.executeSql('CREATE TABLE IF NOT EXISTS previsao_tempo (id INTEGER PRIMARY KEY AUTOINCREMENT, value text, date_created DATETIME);');
    // tx.executeSql('SELECT * FROM previsao_tempo', [], (_, { rows }) => {
    //     if(rows.length > 10){
    //         tx.executeSql('DELETE FROM cotacao')
    //     }
    // });
    //tx.executeSql('DROP TABLE previsao_tempo');
});

export function getPrevisaTempo(gpsLocation) {
    return async function(dispatch){

        console.log(gpsLocation)

        db.transaction((tx) => {
            tx.executeSql('SELECT * FROM previsao_tempo ORDER BY id DESC LIMIT 1', [], (_, { rows }) => {
                if(rows.length > 0 && moment() > moment(rows._array[0].date_created).add(5, 's')){                    
                    requestTempo();
                }else if(rows.length == 0){
                    requestTempo();
                }else{
                    tx.executeSql('SELECT * FROM previsao_tempo ORDER BY id DESC LIMIT 1', [], (_, { rows }) => {
                        dispatch({type: 'GET_PREVISAO_TEMPO', payload: JSON.parse(rows._array[0].value)});
                    });
                }
            });
        }, (err) => console.error(err))
        
        async function requestTempo(){
            const netInfo = await NetInfo.getConnectionInfo();
            if(netInfo.type == 'wifi' || netInfo.type == 'cellular'){
                axios.get(`previsaoTempo/previsaoTempo?lat=${gpsLocation.latitude}&lon=${gpsLocation.longitude}`)
                .then((result) => {
                    db.transaction((tx) => {
                        tx.executeSql('SELECT * FROM previsao_tempo', [], (_, { rows }) => {
                            if(rows.length > 5){
                                tx.executeSql('DELETE FROM previsao_tempo')
                            }
                        });
                    })
                    return result.data;
                })
                .then((result) => {                    
                    db.transaction((tx) => {
                        tx.executeSql('INSERT INTO previsao_tempo (value, date_created) values (?, ?)', [JSON.stringify(result), moment().format('YYYY-MM-DD HH:mm:ss').toString()]);
                    }, (err) => console.log(err));
                    return result;
                })
                .then((result) => dispatch({type: 'GET_PREVISAO_TEMPO', payload: result}))
                .catch((err) => console.error(err))
            }else{
                db.transaction(
                    (tx) => {
                        tx.executeSql('SELECT * FROM previsao_tempo ORDER BY id DESC LIMIT 1', [], (_, { rows }) => {
                            dispatch({type: 'GET_PREVISAO_TEMPO', payload: JSON.parse(rows._array[0].value)});
                        });
                    },
                );
            }
        }
    }
}