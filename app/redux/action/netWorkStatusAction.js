import { API } from '../const';
import { NetInfo } from 'react-native';

export default function() {
	return function(dispatch){
		NetInfo.addEventListener('connectionChange', (connectionInfo) => dispatch({type: 'NETWORK_STATUS', payload: connectionInfo}));
	}
}
