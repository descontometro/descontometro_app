import Expo, { SQLite } from 'expo';
import axios from 'axios';
import { NetInfo } from 'react-native';

export function getCuponsAction() {
    return async function(dispatch){
      axios.get('coupons')
      .then((result) => {
          dispatch({type: 'COUPONS_LIST', payload: result.data});
      })
      .catch((err) => {
          console.error(err)
      })
    }
}
export function getSelectCupom(id) {
    return async function(dispatch){

      axios.get(`coupons/${id}`)
      .then((result) => {
          dispatch({type: 'COUPONS_LIST_INFO', payload: result.data});
      })
      .catch((err) => {
          console.error(err)
      })
    }
}
