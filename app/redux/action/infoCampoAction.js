import Expo, { SQLite } from 'expo';
import axios from 'axios';
import { NetInfo } from 'react-native';

const db = SQLite.openDatabase('vaccaro.db');

db.transaction(tx => {
    tx.executeSql('CREATE TABLE IF NOT EXISTS listInfoCampo (id INTEGER PRIMARY KEY AUTOINCREMENT, value TEXT);');    
    //tx.executeSql('DROP TABLE listInfoCampo');
});

db.transaction(tx => {
    tx.executeSql('CREATE TABLE IF NOT EXISTS getSelectInfoCampo (id INTEGER, value TEXT);');    
    //tx.executeSql('DROP TABLE getSelectInfoCampo');
});

db.transaction(tx => {
    tx.executeSql('CREATE TABLE IF NOT EXISTS getTiposInfoCampo (id INTEGER PRIMARY KEY AUTOINCREMENT, value TEXT);');    
    //tx.executeSql('DROP TABLE getSelectInfoCampo');
});

export function listInfoCampo() {     
    return async function(dispatch){

        const netInfo = await NetInfo.getConnectionInfo();

        if(netInfo.type == 'wifi' || netInfo.type == 'cellular'){
            axios.get('/infoCampo/listInfoCampoApp')
            .then((result) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT * FROM listInfoCampo', [], (_, { rows }) => {
                        if(rows.length > 2){
                            tx.executeSql('DELETE FROM listInfoCampo')
                        }
                    });
                },  (err) => console.error(err));

                return result.data;
            })
            .then((result) => {
                db.transaction((tx) => {
                    tx.executeSql('INSERT INTO listInfoCampo (value) values (?)', [JSON.stringify(result)]);
                },  (err) => console.error(err));
                return result;
            })
            .then((result) => dispatch({type: 'LIST_INFO_CAMPO', payload: result}))
            .catch((err) => console.error(err))
        }else{
            db.transaction(
                (tx) => {
                    tx.executeSql('SELECT * FROM listInfoCampo ORDER BY id DESC LIMIT 1', [], (_, { rows }) => {
                        dispatch({type: 'LIST_INFO_CAMPO', payload: JSON.parse(rows._array[0].value)});
                    });
                },
            );
        }
    }
}

export function listTiposInfoCampo() {     
    return async function(dispatch){

        const netInfo = await NetInfo.getConnectionInfo();

        if(netInfo.type == 'wifi' || netInfo.type == 'cellular'){
            axios.get('/infoCampo/listTipoEventApp')
            .then((result) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT * FROM getTiposInfoCampo', [], (_, { rows }) => {
                        if(rows.length > 2){
                            tx.executeSql('DELETE FROM getTiposInfoCampo')
                        }
                    });
                },  (err) => console.error(err));

                return result.data;
            })
            .then((result) => {
                db.transaction((tx) => {
                    tx.executeSql('INSERT INTO getTiposInfoCampo (value) values (?)', [JSON.stringify(result)]);
                },  (err) => console.error(err));
                return result;
            })
            .then((result) => dispatch({type: 'LIST_TIPO_INFO_CAMPO', payload: result}))
            .catch((err) => console.error(err))
        }else{
            db.transaction(
                (tx) => {
                    tx.executeSql('SELECT * FROM getTiposInfoCampo ORDER BY id DESC LIMIT 1', [], (_, { rows }) => {
                        dispatch({type: 'LIST_TIPO_INFO_CAMPO', payload: JSON.parse(rows._array[0].value)});
                    });
                },
            );
        }
    }
}

export function getSelectInfoCampo(id) {     
    return async function(dispatch){

        const netInfo = await NetInfo.getConnectionInfo();

        if(netInfo.type == 'wifi' || netInfo.type == 'cellular'){            
            axios.get(`/infoCampo/selectInfoCampoApp?id=${id}`)
            .then((result) => {
                for(let i = 0; i < result.data[0].images.length;i++){
                    let image = result.data[0].images[i];
    
                    image.thumb_info_campo_galeria = `https://vaccaro.o2.ag/upload/${image.thumb_info_campo_galeria}`
                    image.maior_info_campo_galeria = `https://vaccaro.o2.ag/upload/${image.maior_info_campo_galeria}`
                }
                if(result.data[0].video_info_campo){
                    let splitYoutube = result.data[0].video_info_campo.split('/');
                    result.data[0].video_info_campo = `https://www.youtube.com/embed/${splitYoutube[3].split('=')[1]}?rel=0&autoplay=0&showinfo=0&controls=0`
                }
                return result;
            })
            .then((result) => {
                db.transaction(
                    (tx) => {
                        tx.executeSql('SELECT * FROM getSelectInfoCampo WHERE id = ?', [id], (_, { rows }) => {
                            if(rows.length > 0){
                                tx.executeSql('DELETE FROM getSelectInfoCampo WHERE id = ?', [id])
                            }
                        });
                        tx.executeSql('SELECT * FROM getSelectInfoCampo', [], (_, { rows }) => {
                            if(rows.length > 30){
                                tx.executeSql('DELETE FROM getSelectInfoCampo')
                            }
                        });
                    }
                );
                return result;
            })
            .then((result) => {
                db.transaction(
                    (tx) => {                                     
                        tx.executeSql('INSERT INTO getSelectInfoCampo (id, value) values (?, ?)', [result.data[0].id_info_campo, JSON.stringify(result.data)]);
                    }
                );
                return result;
            })
            .then((result) => dispatch({type: 'GET_INFO_CAMPO', payload: result.data}))
            .catch((err) => console.error(err))
        }else{
            db.transaction(
                (tx) => {
                    tx.executeSql('SELECT * FROM getSelectInfoCampo WHERE id = ? ORDER BY id DESC LIMIT 1', [id], (_, { rows }) => {
                        console.warn(rows._array)
                        if(rows.length > 0){
                            dispatch({type: 'GET_INFO_CAMPO', payload: JSON.parse(rows._array[0].value)});
                        }else{
                            dispatch({type: 'NO_DATA_OFFLINE'})
                        }
                    });
                },
            );
        }
    }
}