import { combineReducers } from 'redux';

import sideMenu from './sideMenu';
import login from './loginReducer';
import cotacao from './cotacaoReducer';
import cupons from './cuponsReducer';
import previsaoTempo from './previsaoTempoReducer';
import infoCampo from './infoCampoReducer';

export default combineReducers({
	sideMenu,
	login,
	cotacao,
	previsaoTempo,
	infoCampo,
	cupons
});
