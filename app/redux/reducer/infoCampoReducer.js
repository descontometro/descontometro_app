import Immutable from 'immutable';

const initialState = new Immutable.Map({
    listInfoCampo: [],
    selectedInfoCampo: [],
    listTiposInfoCampo: [],
    isOffline: null
  });

export default function reducer(state = initialState, action){
	switch (action.type) {
        case 'LIST_INFO_CAMPO':
            return state.merge({listInfoCampo: action.payload.sort((a,b) => b.id_info_campo-a.id_info_campo).splice(0,40)})
        break;

        case 'GET_INFO_CAMPO':
			return state.merge({selectedInfoCampo: action.payload})
        break;

        case 'LIST_TIPO_INFO_CAMPO':
			return state.merge({listTiposInfoCampo: action.payload})
        break;

        case 'DESTROY_INFO_CAMPO':
            return state.merge({selectedInfoCampo: []})
        break;

        case 'NO_DATA_OFFLINE':
            return state.merge({isOffline: action.type})
        break;

        default: 
            return state;
	}	
}
