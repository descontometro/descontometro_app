export default function reducer(state={
    listMilho: [],
    listTrigo: [],
    listSoja: [],
    listCotacaoHome: [
        {
            titulo: 'Milho',
            valor: null
        },
        {
            titulo: 'Trigo',
            valor: null
        },
        {
            titulo: 'Soja',
            valor: null
        }
    ]
},action){
	switch (action.type) {
        case 'LIST_MILHO':            
            state.listCotacaoHome[0].valor = action.payload[0].valor;
			return {...state, listMilho: action.payload}
        break;
        
        case 'LIST_TRIGO':
            state.listCotacaoHome[1].valor = action.payload[0].valor;
			return {...state, listTrigo: action.payload}
        break;
        
        case 'LIST_SOJA':
            state.listCotacaoHome[2].valor = action.payload[0].valor;
			return {...state, listSoja: action.payload}
		break;
	}
	return state;
}
