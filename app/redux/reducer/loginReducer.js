export default function reducer(state={
	dadosUsuario: [],
	isAutorized: false
},action){
	switch (action.type) {
		case 'POST_LOGIN':
			return {...state, isAutorized: true, dadosUsuario: action.payload}
		break;

		case 'LOGIN_DENIED':		
			return {...state, isAutorized: false, dadosUsuario: action.payload}
		break;

		case 'USER_LOGOUT':
			return {...state, isAutorized: false, dadosUsuario: []}
		break;
	}
	return state;
}
