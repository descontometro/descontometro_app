import React from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, BackHandler, NetInfo, Image, Dimensions } from 'react-native';
import { DrawerNavigator, NavigationActions, StackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

import { Header } from '../component/'

import Login from '../layout/login/login';
import Home from '../layout/home/home';
import Cupom from '../layout/cupom/cupom';

const styles = StyleSheet.create({
	touchContainer: {
		padding: 20,
		borderBottomWidth: 1,
		borderColor: '#dadada'
	},
	touchMenu: {
		flex: 0,
		flexDirection: 'row',
		alignItems: 'center'
	},
	touchText: {
		fontFamily: 'raleway-semi-bold',
		fontSize: 18,
		color: '#44BA09',
		marginLeft: 20
	}
});

class NewLayout extends React.Component {
	constructor(props){
		super(props);
	}

	componentDidMount() {
		BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid);
	}

	componentWillUnmount() {

	}

	componentWillMount(){

	}

	componentWillReceiveProps(nextProps){

	}

	onBackButtonPressAndroid = () => {
		const { dispatch, sideMenu } = this.props;

		if (sideMenu.routes[1].routes[0].index === 0) {
			return false;
		}

		dispatch(NavigationActions.back());
		return true;
	}
	render(){
		const { navigation, user, dispatch } = this.props;
		return (
			<ScrollView style={{backgroundColor: '#FFF', flex: 1, borderTopWidth: 1, borderColor: '#FFF'}}>
				<View style={[styles.touchContainer]}>
					<TouchableOpacity style={styles.touchMenu} onPress={() => this.props.navigation.navigate('HomeScreen')}>
						<Icon name="usd" color="#44BA09" size={25} />
						<Text style={styles.touchText}>Descontos</Text>
					</TouchableOpacity>
				</View>


				<TouchableOpacity
					style={[styles.touchMenu, {borderWidth: 2, borderColor: '#44BA09', paddingVertical: 15, borderRadius: 30, justifyContent: 'center', margin: 25}]}
					onPress={() => this.props.navigation.navigate('Login')}>
					<Icon name="send" size={25} color="#44BA09" />
					<Text style={styles.touchText}>Login</Text>
				</TouchableOpacity>
			</ScrollView>
		)
	}
}

const mapStateToProps = function(state){
  return {
    user: state.login,
		sideMenu: state.sideMenu,
  }
}

let SideBarRedux = connect(mapStateToProps)(NewLayout);

const slideMenu = DrawerNavigator(
	{
		HomeScreen: {
			screen: Home
		},
		Login: {
			screen: Login
		},
		CupomScreen: {
			screen: Cupom
		},
	},
	{
		drawerWidth: Dimensions.get('window').width - 25,
		contentComponent: (props) => <SideBarRedux {...props} />
	}
);

export default StackNavigator(
	{
		HomeScreen: {
			screen: Home
		},
		slideMenu: {
			screen: slideMenu,
		},
		CupomScreen: {
			screen: Cupom
		},
	},
	{
		initialRouteName: 'HomeScreen',
		headerMode: 'float',
		navigationOptions: {
			header: (props) => <Header {...props} />,
		}
	}
);
