import React from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet, Text, Image, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';

class Header extends React.Component {

	constructor(props){
		super(props);

		this.state = {changeDrawer: 'DrawerClose'}
	}

	componentDidMount(){

	}

	componentWillMount(){

	}

	_toogleDrawer = () => {
		let { changeDrawer } = this.state
		if(changeDrawer == 'DrawerOpen'){
			this.setState({changeDrawer: 'DrawerClose'})
		}else{
			this.setState({changeDrawer: 'DrawerOpen'})
		}
		this.props.navigation.navigate(this.state.changeDrawer)
	}

	render(){
		const { sizeContainer, sizeIcoBar } = {...this.state};

		return (
			<View elevation={5} style={styles.container}>
				<TouchableOpacity style={{position: 'absolute', left: 0, top: -10, paddingHorizontal: 20, height: 100, justifyContent: 'center'}} onPress={() => this._toogleDrawer()}>
					<Icon name="bars" color="#44BA09" size={25} />
				</TouchableOpacity>

				<TouchableOpacity onPress={() => this.props.navigation.navigate('HomeScreen')}>
					<Image source={require('../../assets/marca-login.png')} style={styles.image} />
				</TouchableOpacity>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 0,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		backgroundColor: '#FFF',
		paddingVertical: 10,
		height: 80,
		shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1.0
	},
	icoSideMenu: {
		position: 'absolute',
		alignItems: 'center',
		top: 0,
		width: 70,
		shadowColor: '#000000'
	},
	image: {
		top: 5,
	 	width: 80,
	  resizeMode: Image.resizeMode.contain
	},
});

const mapStateToProps = function(state){
	return {}
}

export default connect(mapStateToProps)(Header);
