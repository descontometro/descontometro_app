import React from 'react';
import { View, TextInput, StyleSheet, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types';

class InputText extends React.Component {

	constructor(props){
		super(props);

		this.state = {icoDimensions: PropTypes.object, viewDimensions: PropTypes.object};
	}

	componentDidMount(){

	}

	render(){

		const {icoDimensions, viewDimensions} = {...this.state};

		let cssInput = {
			paddingVertical: 15,
			paddingHorizontal: 15,
			color: '#FFF',
			backgroundColor: 'rgba(255,255,255,.7)',
			borderRadius: 5,
			elevation: 5,
			...this.props.style,
		}

		let cssIcon = {
			position: 'absolute',
			top: (viewDimensions.height - icoDimensions.height) / 2,
			left: 15,
			...this.props.iconStyle
		};

		if(this.props.icon){
			cssInput.paddingLeft = 50;
		}

		return (
			<View onLayout={(event) => this.setState({viewDimensions: event.nativeEvent.layout})}>
				{viewDimensions && this.props.icon && <Icon onLayout={(event) => this.setState({icoDimensions: event.nativeEvent.layout})} name={this.props.icon.name} size={this.props.icon.size} style={cssIcon} />}
				<TextInput {...this.props} style={cssInput} underlineColorAndroid="transparent" />
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		flexDirection: 'column',
		backgroundColor: '#003e79'
	}
});

export default InputText;
