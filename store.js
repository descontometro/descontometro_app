import { applyMiddleware, createStore, replaceReducer } from 'redux';

//import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';

import reducer from './app/redux/reducer/index';

//const middleware = applyMiddleware(promise(), thunk);
const middleware = applyMiddleware(promise(), thunk);

export default initialState => {

    //const logger = createLogger({collapsed: true});

    const store = createStore(
        reducer,
        initialState,
        applyMiddleware(promise(), thunk)
    );

    if (module.hot) {
        module.hot.accept(() => {
            const nextRootReducer = require('./app/redux/reducer').default;
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;
};
